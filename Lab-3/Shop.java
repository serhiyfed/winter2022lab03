import java.util.Scanner;

public class Shop {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Album[] albums = new Album[4];
        for (int i = 0; i < albums.length; i++) {
            albums[i] = new Album();
            albums[i].cost = sc.nextDouble();
            sc.nextLine();
            albums[i].pubYear = sc.nextInt();
            sc.nextLine();
            albums[i].title = sc.nextLine();
        }

        //softcoded
        System.out.println(albums[albums.length - 1].cost);
        System.out.println(albums[albums.length - 1].pubYear);
        System.out.println(albums[albums.length - 1].title);
        System.out.println(albums[albums.length - 1].getCostAfterTax());
    }
}
